/*
1) Boolean, String, Number, Object, BigInt, Symbol, null, undefined.
2) ==   -  нестроге порівняння. Цей оператор приводить операнди до типу Number і порівнює їх.
   ===  -  строге порівняння. Не приводить операнди до одного типу і перевіряє рівність.
3) Оператор - це спеціальний символ у JavaScript, який виконує ту, чи іншу дію з операндами:
+, -, /, *, >, >=, <, <=  і тд.
Оператори можуть бути унарними: a++, --a  і тд.
Оператори можуть бути бінарними: a + b, a / b  і тд.
*/

// 

let userName = prompt('Введіть Ваше ім"я', 'Alex')

while (!isNaN(userName)) {
  alert('Поле не може бути пустим, або Ви некоректно ввели дані')
  userName = prompt('Введіть Ваше ім"я', 'Alex')
}

let userAge = +prompt('Введіть Ваш вік', 24)

while (isNaN(userAge) || userAge == false) {
  alert('Поле не може бути пустим, або Ви некоректно ввели дані')
  userAge = +prompt('Введіть Ваш вік', 24)
}

if (userAge < 18) {
  alert(`You are not allowed to visit this website`)
} else if (userAge >= 18 && userAge <= 22) {
  const userConfirm = confirm('Are you sure you want to continue?')
  if (userConfirm) {
    alert(`Welcome, ${userName}!`)
  } else {
    alert('You are not allowed to visit this website')
  }
} else {
  alert(`Welcome, ${userName}!`)
}
